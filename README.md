# Recruitment assignment #

Assignee is supposed to write a programme which for the given input file will produce correct output.

## The dressing up problem ##

The given file is made of lines. Each represents the correct order of dressing up.
```properties
Pants Belt
Shirt Jacket
Shirt Tie
Pants Jacket
Pants Shoes
Jacket Shoes
Jacket Coat

```
The example line:
```properties
Shirt Tie
```
tells, that `Shirt` has to be dressed up **before** `Tie`. 

Given that, we want to answer the following questions:

1) How many clothes have to be dressed up before the `Jacket` can be put on?   
2) What is the correct order of dressing up?

## General requirements ##
The candidate should not use external help.

## How do I get set up? ##
The candidate is **not** limited to `Java` language. The solution can be 
provided in any programming language.

However, the default setup is organized in a form of a `Gradle` project. The candidate can import 
the project to a favourite IDE and start coding in `Java` right after that.

### Technical aspects ###
This project was set up to make sure, that the assignee will 
focus on solving the problem rather than configuring the environment.

The project's classpath hase `JUnit` dependency.

`<ProjectRoot>/src/main/java/Solution.java` is the file in which the solution to the problem has to be placed.

`<ProjectRoot>/src/main/java/SolutionTest.java` provides a test cases. 

## After the test
The candidate should push the changes to a separate Git branch.